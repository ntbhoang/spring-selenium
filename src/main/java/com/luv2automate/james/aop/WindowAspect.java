package com.luv2automate.james.aop;


import com.luv2automate.james.annotations.Window;
import com.luv2automate.james.service.SwitchWindowService;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class WindowAspect {


    @Autowired
    private SwitchWindowService windowService;


    // Any class that has the annotation @Window and is within the package `com.luv2automate`
    @Before("@target(window) && within(com.luv2automate..*)")
    public void before(Window window){
        this.windowService.switchWindowByTitle(window.value());
    }


    @After("@target(window) && within(com.luv2automate..*)")
    public void after(Window window){
        this.windowService.switchWindowById(0);
    }
}
