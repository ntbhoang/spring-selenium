package com.luv2automate.james.service;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class SwitchWindowService {

    @Autowired
    private ApplicationContext applicationContext;


    public void switchWindowByTitle(final String title){
        WebDriver driver = applicationContext.getBean(WebDriver.class);
        driver.getWindowHandles()
                .stream()
                .map(handle -> driver.switchTo().window(handle).getTitle())
                .filter(t -> t.startsWith(title))
                .findFirst()
                .orElseThrow(() -> {
                    throw new RuntimeException("No such window with :: " + title);
                });
    }

    public void switchWindowById(final int index){
        WebDriver driver = applicationContext.getBean(WebDriver.class);
        String handles[] = driver.getWindowHandles().toArray(new String[0]);
        driver.switchTo().window(handles[index]);
    }
}
