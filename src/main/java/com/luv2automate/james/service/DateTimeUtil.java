package com.luv2automate.james.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public final class DateTimeUtil {

    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy-MM-dd:HH.mm.ss.SSS");

    public static String getCurrentDateTime(){
        var localDateTime = LocalDateTime.now();
        return formatter.format(localDateTime);
    }

    public static String getDateTimeStringFrom(){
        String dateTimeString = getCurrentDateTime();
        return dateTimeString.replaceAll(" ", "").trim();
    }
}
