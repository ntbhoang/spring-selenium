package com.luv2automate.james.service;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

// By default, Spring Framework will scan all the components then create all the beans in advance though you're not going to use the bean
// That kind create a performance issue - therefore, you can add `@Lazy` annotation before class and before `@Autowired`

@Lazy
@Service
public class ScreenShotUtil {

    @Autowired
    private ApplicationContext context;

    @Value("${application.destPath}")
    private Path destPath;


    private void copyFile(File srcFile, File destFile){
        try{
            FileCopyUtils.copy(srcFile, destFile);
        } catch (IOException e) {
            System.out.println("Cannot copy file from " + srcFile + " to " + destFile);
        }
    }

    public void takeScreenShot(){
        var destFile = destPath.resolve(DateTimeUtil.getDateTimeStringFrom() + ".png").toFile();
        var scrShot = context.getBean(TakesScreenshot.class);
        var srcFile = scrShot.getScreenshotAs(OutputType.FILE);
        copyFile(srcFile, destFile);
    }
}
