package com.luv2automate.james.config;

import com.luv2automate.james.annotations.LazyConfiguration;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.*;

import java.net.URL;


@LazyConfiguration
public class RemoteWebDriverConfig {


    @Value("${selenium.grid.url}")
    private URL url;


    @Bean
    public WebDriver remoteFirefoxDriver(){
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setBrowserName("firefox");
        capabilities.setPlatform(Platform.MAC);

        return new RemoteWebDriver(this.url, capabilities);
    }

    @Bean
    public WebDriver remoteChromeDriver(){
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setBrowserName("chrome");
        capabilities.setPlatform(Platform.MAC);

        return new RemoteWebDriver(this.url, capabilities);
    }

}
