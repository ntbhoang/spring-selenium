package com.luv2automate.james.config;

import com.luv2automate.james.annotations.LazyConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;


@LazyConfiguration
public class WebDriverWaitConfig {

    @Value("${application.timeout}")
    private int timeout;


    @Bean
    @Scope("prototype")
    public WebDriverWait webDriverWait(WebDriver driver){
        return new WebDriverWait(driver, this.timeout);
    }
}
