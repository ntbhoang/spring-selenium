package com.luv2automate.pages.visa;

import com.luv2automate.james.annotations.Page;
import com.luv2automate.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.time.LocalDate;
import java.util.Objects;


@Page
public class VisaRegistrationPage extends BasePage {

    @FindBy(id = "first_4")
    private WebElement firstName;

    @FindBy(id = "last_4")
    private WebElement lastName;


    @FindBy(id = "input_46")
    private WebElement fromCountry;


    @FindBy(id = "first_47")
    private WebElement toCountry;


    @FindBy(id = "input_24_month")
    private WebElement month;

    @FindBy(id = "input_24_day")
    private WebElement day;

    @FindBy(id = "input_24_year")
    private WebElement year;

    @FindBy(id = "input_6")
    private WebElement email;

    @FindBy(id = "input_27")
    private WebElement phone;

    @FindBy(id = "input_45")
    private WebElement comments;

    @FindBy(id = "submitBtn")
    private WebElement submitButton;

    @FindBy(id = "requestNumber")
    private WebElement requestNumber;

    public void goTo(){
        this.driver.get("https://vins-udemy.s3.amazonaws.com/sb/visa/udemy-visa.html");
    }

    public void setName(String firstName, String lastName){
        this.firstName.sendKeys(firstName);
        this.lastName.sendKeys(lastName);
    }

    public void setCountry(String fromCountry, String toCountry){
        new Select(this.fromCountry).selectByValue(fromCountry);
        new Select(this.toCountry).selectByValue(toCountry);
    }

    public void setBirthDate(LocalDate localDate){
        new Select(this.year).selectByVisibleText(String.valueOf(localDate.getYear()));
        new Select(this.day).selectByVisibleText(String.valueOf(localDate.getDayOfMonth()));
        new Select(this.month).selectByValue(localDate.getMonth().toString());
    }

    public void setContactDetails(String email, String phone){
       this.email.sendKeys(email);
       this.phone.sendKeys(phone);
    }

    public void setComments(String comments){
        this.comments.sendKeys(Objects.toString(comments), "");
    }

    public void submit(){ this.submitButton.click();}

    public String getConfirmationNumber(){
        this.wait.until(d -> this.requestNumber.isDisplayed());
        return this.requestNumber.getText();
    }


    @Override
    public boolean isAt() {
        return this.wait.until(d -> this.firstName.isDisplayed());
    }
}
