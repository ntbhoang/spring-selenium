package com.luv2automate.pages;

import com.google.common.util.concurrent.Uninterruptibles;
import com.luv2automate.james.annotations.PageFragment;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.concurrent.TimeUnit;

@PageFragment
public class SearchComponent extends BasePage{

    @FindBy(name = "q")
    private WebElement searchTextBox;


    @FindBy(name = "btnK")
    private List<WebElement> searchButtons;


    @Override
    public boolean isAt() {
        return this.wait.until((e -> this.searchTextBox.isDisplayed()));
    }

    public void search(final String keyword){
        searchTextBox.sendKeys(keyword);
        searchTextBox.sendKeys(Keys.TAB);
        Uninterruptibles.sleepUninterruptibly(500, TimeUnit.MILLISECONDS);
        searchButtons.stream()
                .filter(ele -> ele.isDisplayed() && ele.isEnabled())
                .findFirst()
                .ifPresent(WebElement::click);
    }
}
