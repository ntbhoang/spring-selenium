package com.luv2automate.pages;

import com.luv2automate.james.annotations.PageFragment;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


@PageFragment
public class SearchResultComponent extends BasePage{

    @FindBy(id = "result-stats")
    private WebElement resultStats;


    @FindBy(tagName = "a")
    private List<WebElement> links;

    private String getResultStats(){
        return this.resultStats.getText();
    }


    public long getResultStatsNumber(){
        String resultString = getResultStats();
        System.out.println(resultString);
        String finalStr = resultString.substring(6, resultString.indexOf("k"));
        return Long.parseLong(finalStr.replace(".", "").trim());
    }


    @Override
    public boolean isAt() {
        return this.wait.until(d -> this.resultStats.isDisplayed());
    }
}
