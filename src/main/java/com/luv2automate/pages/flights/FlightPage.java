package com.luv2automate.pages.flights;

import com.luv2automate.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class FlightPage extends BasePage {

    @FindBy(xpath = "//span[@jsname='iSelEd']")
    private List<WebElement> flightOptions;


    @Override
    public boolean isAt() {
        return this.flightOptions.stream()
                .findAny()
                .isPresent();
    }


    public void goTo(String url){
        this.driver.get(url);
    }


    public List<String> getLocaleLanguage(){
        return this.flightOptions.stream()
                .map(WebElement::getText)
                .map(String::trim)
                .collect(Collectors.toList());
    }

}
