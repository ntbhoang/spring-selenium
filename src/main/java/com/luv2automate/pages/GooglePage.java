package com.luv2automate.pages;

import com.luv2automate.james.annotations.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@Page
public class GooglePage extends BasePage{

    @Autowired
    private SearchComponent searchComponent;

    @Autowired
    private SearchResultComponent searchResultComponent;

    @Value("${application.url}")
    private String url;


    public SearchComponent getSearchComponent() {
        return searchComponent;
    }


    public SearchResultComponent getSearchResultComponent() {
        return searchResultComponent;
    }


    public void goTo(){
        this.driver.get(url);
    }

    public void close(){
        this.driver.quit();
    }


    @Override
    public boolean isAt() {
        return this.wait.until(d -> this.searchComponent.isAt());
    }
}
