package com.luv2automate.pages.windows;

import com.luv2automate.james.annotations.Page;
import com.luv2automate.james.annotations.Window;
import com.luv2automate.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Window("Page B")
public class PageB extends BasePage {

    @FindBy(id = "area")
    private WebElement textArea;


    public void enterMessage(final String msg){
        this.textArea.sendKeys(msg);
    }

    @Override
    public boolean isAt() {
        return this.wait.until(d -> this.textArea.isDisplayed());
    }
}
