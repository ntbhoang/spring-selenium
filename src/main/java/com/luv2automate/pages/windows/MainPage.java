package com.luv2automate.pages.windows;

import com.google.common.util.concurrent.Uninterruptibles;
import com.luv2automate.james.annotations.Page;
import com.luv2automate.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.sql.Time;
import java.util.List;
import java.util.concurrent.TimeUnit;


@Page
public class MainPage extends BasePage {

    @FindBy(tagName = "a")
    private List<WebElement> links;

    public void goTo(){
        this.driver.get("https://vins-udemy.s3.amazonaws.com/ds/window/main.html");
    }


    public void launchWindows(){

        links.forEach(link -> {
            link.click();
            Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
        });
    }


    @Override
    public boolean isAt() {
        return this.wait.until(d -> !this.links.isEmpty());
    }
}
