package com.luv2automate.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Objects;


public abstract class BasePage {

    @Autowired
    protected WebDriver driver;

    @Autowired
    protected WebDriverWait wait;

    public BasePage(){ }

    @PostConstruct
    private void init(){
        PageFactory.initElements(this.driver, this);
        driver.manage().window().maximize();
    }


    @PreDestroy
    private void tearDown(){
        if(Objects.nonNull(driver)) driver.quit();
    }

    public abstract boolean isAt();
}
