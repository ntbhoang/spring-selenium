package com.luv2automate.tests.flights;


import com.luv2automate.james.annotations.Page;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

@Page
//@PropertySource("languages/${app.locale}.properties")
public class FlightAppDetails {

    @Value("${flight.app.url}")
    private String url;

    @Value("${flight.app.labels}")
    private List<String> labels;


    public String getUrl(){ return url; }


    public List<String> getLabels(){ return labels; }
}
