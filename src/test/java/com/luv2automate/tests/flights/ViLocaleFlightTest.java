package com.luv2automate.tests.flights;


import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = {"flights.locale = vi", "browser=chrome"})
public class ViLocaleFlightTest extends FlightTest{
}
