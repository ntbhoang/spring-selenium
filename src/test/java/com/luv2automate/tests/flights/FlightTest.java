package com.luv2automate.tests.flights;

import com.luv2automate.pages.flights.FlightPage;
import com.luv2automate.tests.SpringBaseTestNG;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;


public class FlightTest extends SpringBaseTestNG {

    @Autowired
    private FlightPage flightPage;


    @Autowired
    private FlightAppDetails appDetails;


    @Test
    public void flightLocaleLanguageTest(){
        this.flightPage.goTo(this.appDetails.getUrl());
        Assert.assertTrue(this.flightPage.isAt());
        Assert.assertEquals(this.flightPage.getLocaleLanguage(), this.appDetails.getLabels());
    }
}
