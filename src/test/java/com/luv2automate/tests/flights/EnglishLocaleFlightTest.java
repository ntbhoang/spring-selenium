package com.luv2automate.tests.flights;


import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = "flights.locale = en")
public class EnglishLocaleFlightTest extends FlightTest{
}
