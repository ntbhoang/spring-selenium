package com.luv2automate.tests.resource;

import com.luv2automate.tests.SpringBaseTestNG;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ResourceTest extends SpringBaseTestNG {


    @Value("classpath:data/test_data.csv")
    private Resource resource;


    @Value("https://www.google.com")
    private Resource httpResource;


    @Value("${application.httpSavedPath}/saved_data.txt")
    private Path httpSavedPath;



    @Test
    public void localResourceTest() throws IOException {
        Files.readAllLines(resource.getFile().toPath()).forEach(System.out::println);
    }

    @Test
    public void getHttpResourceTest() throws IOException {
        System.out.println(new String(httpResource.getInputStream().readAllBytes()));
    }


    @Test
    public void saveHttpResourceIntoFileTest() throws IOException {
       //Files.copy( httpResource.getInputStream().transferTo(Files.newOutputStream(httpSavedPath)));
    }
}
