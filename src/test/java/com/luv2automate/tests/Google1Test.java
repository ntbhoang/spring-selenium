package com.luv2automate.tests;

import com.google.common.util.concurrent.Uninterruptibles;
import com.luv2automate.james.service.ScreenShotUtil;
import com.luv2automate.pages.GooglePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Google1Test extends SpringBaseTestNG{

    @Autowired
    private GooglePage googlePage;


    @Autowired
    private ScreenShotUtil shotManager;


    @Test
    public void googleSearchTest(){
        googlePage.goTo();
        Assert.assertTrue(googlePage.isAt());
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
        googlePage.getSearchComponent().search("Sring boot");
        long expectedResult = googlePage.getSearchResultComponent().getResultStatsNumber();
        shotManager.takeScreenShot();
        googlePage.close();
        Assert.assertEquals(148000000, expectedResult);
    }
}
