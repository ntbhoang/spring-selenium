package com.luv2automate.tests;

import com.google.common.util.concurrent.Uninterruptibles;
import com.luv2automate.james.service.ScreenShotUtil;
import com.luv2automate.pages.GooglePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Google2Test extends SpringBaseTestNG{

    @Autowired
    private GooglePage googlePage;

    @Lazy
    @Autowired
    private ScreenShotUtil shotManager;


    @Test
    public void googleSearchTest(){
        googlePage.goTo();
        Assert.assertTrue(googlePage.isAt());
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
        googlePage.getSearchComponent().search("Selenium ");
        long expectedResult = googlePage.getSearchResultComponent().getResultStatsNumber();
        shotManager.takeScreenShot();
        googlePage.close();
        Assert.assertEquals(148000000, expectedResult);
    }
}
