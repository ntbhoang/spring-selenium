package com.luv2automate.tests.windows;

import com.luv2automate.james.service.SwitchWindowService;
import com.luv2automate.pages.windows.MainPage;
import com.luv2automate.pages.windows.PageA;
import com.luv2automate.pages.windows.PageB;
import com.luv2automate.pages.windows.PageC;
import com.luv2automate.tests.SpringBaseTestNG;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


@TestPropertySource(properties = "browser = firefox")
public class SwitchWindowTest extends SpringBaseTestNG {

    @Autowired
    private MainPage mainPage;

    @Autowired
    private PageA pageA;

    @Autowired
    private PageB pageB;

    @Autowired
    private PageC pageC;

    @Autowired
    private SwitchWindowService windowService;

    @BeforeClass
    public void setUp(){
        mainPage.goTo();
        mainPage.isAt();
        mainPage.launchWindows();
    }

   /* @Test
    public void switchWindowTest(){
        windowService.switchWindowByTitle("Page A");
        this.pageA.enterMessage("This is Page A");

        windowService.switchWindowById(2);
        this.pageB.enterMessage("This is Page B");

    }*/


    @Test(dataProvider = "getData")
    public void switchWindowTestAOP(int index){
        this.pageA.enterMessage(index + "\n");
        this.pageB.enterMessage(index * 2 + "\n");
        this.pageC.enterMessage(index * 3 + "\n");
    }

    @DataProvider
    public Object[] getData(){
        return new Object[] {
                1,
                4,
                5,
                8
        };
    }
}
