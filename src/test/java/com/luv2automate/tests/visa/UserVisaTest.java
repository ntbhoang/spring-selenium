package com.luv2automate.tests.visa;

import com.luv2automate.james.entity.User;
import com.luv2automate.james.repository.UserRepository;
import com.luv2automate.pages.visa.VisaRegistrationPage;
import com.luv2automate.tests.SpringBaseTestNG;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

public class UserVisaTest extends SpringBaseTestNG {

    @Autowired
    private VisaRegistrationPage registrationPage;

    @Autowired
    private UserRepository repository;


   @Test()
    public void visaTest(){
       List<User> users = this.repository.findByFirstNameStartingWith("Tommie")
               .stream()
               .limit(3)
               .collect(Collectors.toList());

       for(var user : users){
           this.registrationPage.goTo();
           this.registrationPage.setName(user.getFirstName(), user.getLastName());
           this.registrationPage.setCountry(user.getFromCountry(), user.getToCountry());
           this.registrationPage.setBirthDate(user.getDob().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
           this.registrationPage.setContactDetails(user.getEmail(), user.getPhone());
           this.registrationPage.setComments(user.getComments());
           this.registrationPage.submit();
       }
    }

    @Test
    public void repositoryTest(){
        int size = this.repository.findAll().size();
        System.out.println(size);
    }

}
