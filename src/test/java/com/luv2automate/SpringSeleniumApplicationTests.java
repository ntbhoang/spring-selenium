package com.luv2automate;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

class SpringSeleniumApplicationTests {



	@Value("${PATH}")
	private String path;

	@Value("${TEST_URL:https://www.google.com}") // Default value, Spring won't throw an exception error
	private String testUrl;

	@Value("${first_prop}")
	private List<String> prop;

	@Value("${timeout}")
	private List<String> timeout;


	@Autowired
	private Faker faker;

	@Test
	void contextLoads() {

		System.out.println(faker.name().fullName());
		/*System.out.println(this.path);
		System.out.println(this.prop);
		System.out.println(this.timeout);
		System.out.println(this.testUrl);*/
	}

}
